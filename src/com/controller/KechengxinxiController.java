package com.controller;

import java.io.File;
import java.io.IOException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.entity.Kechengxinxi;
import com.server.KechengxinxiServer;
import com.util.PageBean;
import net.sf.json.JSONObject;
import com.util.db;
import java.sql.SQLException;
import java.sql.*;
@Controller
public class KechengxinxiController {
	@Resource
	private KechengxinxiServer kechengxinxiService;


   
	@RequestMapping("addKechengxinxi.do")
	public String addKechengxinxi(HttpServletRequest request,Kechengxinxi kechengxinxi,HttpSession session) throws SQLException{
		Timestamp time=new Timestamp(System.currentTimeMillis());
		
		kechengxinxi.setAddtime(time.toString().substring(0, 19));
		kechengxinxiService.add(kechengxinxi);
		session.setAttribute("backxx", "添加成功");
		session.setAttribute("backurl", request.getHeader("Referer"));
		
		//session.setAttribute("backurl", "kechengxinxiList.do");
		
		return "redirect:postback.jsp";
		//return "redirect:kechengxinxiList.do";
		
		
		
	}
 
//	处理编辑
	@RequestMapping("doUpdateKechengxinxi.do")
	public String doUpdateKechengxinxi(int id,ModelMap map,Kechengxinxi kechengxinxi){
		kechengxinxi=kechengxinxiService.getById(id);
		map.put("kechengxinxi", kechengxinxi);
		return "kechengxinxi_updt";
	}
	
	
	
	
	
//	后台详细
	@RequestMapping("kechengxinxiDetail.do")
	public String kechengxinxiDetail(int id,ModelMap map,Kechengxinxi kechengxinxi){
		kechengxinxi=kechengxinxiService.getById(id);
		map.put("kechengxinxi", kechengxinxi);
		return "kechengxinxi_detail";
	}
//	前台详细
	@RequestMapping("kcxxDetail.do")
	public String kcxxDetail(int id,ModelMap map,Kechengxinxi kechengxinxi){
		kechengxinxi=kechengxinxiService.getById(id);
		map.put("kechengxinxi", kechengxinxi);
		return "kechengxinxidetail";
	}
//	
	@RequestMapping("updateKechengxinxi.do")
	public String updateKechengxinxi(int id,ModelMap map,Kechengxinxi kechengxinxi,HttpServletRequest request,HttpSession session){
		kechengxinxiService.update(kechengxinxi);
		session.setAttribute("backxx", "修改成功");
		session.setAttribute("backurl", request.getHeader("Referer"));
		return "redirect:postback.jsp";
		//String url = request.getHeader("Referer");
		//return "redirect:"+url;
		//return "redirect:kechengxinxiList.do";
	}

//	分页查询
	@RequestMapping("kechengxinxiList.do")
	public String kechengxinxiList(@RequestParam(value="page",required=false)String page,
			ModelMap map,HttpSession session,Kechengxinxi kechengxinxi, String bianhao, String kemu, String chexing, String chepai, String yuyueshiduan, String keshi1,String keshi2, String jiage1,String jiage2, String beizhu, String jiaolian, String issh){
		if(page==null||page.equals("")){
			page="1";
		}
		PageBean pageBean=new PageBean(Integer.parseInt(page), 15);
		Map<String, Object> pmap=new HashMap<String,Object>();
		pmap.put("pageno", pageBean.getStart());
		pmap.put("pageSize", 15);
		
		
		if(bianhao==null||bianhao.equals("")){pmap.put("bianhao", null);}else{pmap.put("bianhao", bianhao);}
		if(kemu==null||kemu.equals("")){pmap.put("kemu", null);}else{pmap.put("kemu", kemu);}
		if(chexing==null||chexing.equals("")){pmap.put("chexing", null);}else{pmap.put("chexing", chexing);}
		if(chepai==null||chepai.equals("")){pmap.put("chepai", null);}else{pmap.put("chepai", chepai);}
		if(yuyueshiduan==null||yuyueshiduan.equals("")){pmap.put("yuyueshiduan", null);}else{pmap.put("yuyueshiduan", yuyueshiduan);}
		if(keshi1==null||keshi1.equals("")){pmap.put("keshi1", null);}else{pmap.put("keshi1", keshi1);}
		if(keshi2==null||keshi2.equals("")){pmap.put("keshi2", null);}else{pmap.put("keshi2", keshi2);}
		if(jiage1==null||jiage1.equals("")){pmap.put("jiage1", null);}else{pmap.put("jiage1", jiage1);}
		if(jiage2==null||jiage2.equals("")){pmap.put("jiage2", null);}else{pmap.put("jiage2", jiage2);}
		if(beizhu==null||beizhu.equals("")){pmap.put("beizhu", null);}else{pmap.put("beizhu", beizhu);}
		if(jiaolian==null||jiaolian.equals("")){pmap.put("jiaolian", null);}else{pmap.put("jiaolian", jiaolian);}
		
		int total=kechengxinxiService.getCount(pmap);
		pageBean.setTotal(total);
		List<Kechengxinxi> list=kechengxinxiService.getByPage(pmap);
		map.put("page", pageBean);
		map.put("list", list);
		session.setAttribute("p", 1);
		return "kechengxinxi_list";
	}
	
	@RequestMapping("kechengxinxiList2.do")
	public String kechengxinxiList2(@RequestParam(value="page",required=false)String page,
			ModelMap map,HttpSession session,Kechengxinxi kechengxinxi, String bianhao, String kemu, String chexing, String chepai, String yuyueshiduan, String keshi1,String keshi2, String jiage1,String jiage2, String beizhu, String jiaolian, String issh,HttpServletRequest request){
		/*if(session.getAttribute("user")==null){
			return "login";
		}*/
		if(page==null||page.equals("")){
			page="1";
		}
		PageBean pageBean=new PageBean(Integer.parseInt(page), 15);
		Map<String, Object> pmap=new HashMap<String,Object>();
		pmap.put("pageno", pageBean.getStart());
		pmap.put("pageSize", 15);
		
		pmap.put("jiaolian", (String)request.getSession().getAttribute("username"));
		if(bianhao==null||bianhao.equals("")){pmap.put("bianhao", null);}else{pmap.put("bianhao", bianhao);}
		if(kemu==null||kemu.equals("")){pmap.put("kemu", null);}else{pmap.put("kemu", kemu);}
		if(chexing==null||chexing.equals("")){pmap.put("chexing", null);}else{pmap.put("chexing", chexing);}
		if(chepai==null||chepai.equals("")){pmap.put("chepai", null);}else{pmap.put("chepai", chepai);}
		if(yuyueshiduan==null||yuyueshiduan.equals("")){pmap.put("yuyueshiduan", null);}else{pmap.put("yuyueshiduan", yuyueshiduan);}
		if(keshi1==null||keshi1.equals("")){pmap.put("keshi1", null);}else{pmap.put("keshi1", keshi1);}
		if(keshi2==null||keshi2.equals("")){pmap.put("keshi2", null);}else{pmap.put("keshi2", keshi2);}
		if(jiage1==null||jiage1.equals("")){pmap.put("jiage1", null);}else{pmap.put("jiage1", jiage1);}
		if(jiage2==null||jiage2.equals("")){pmap.put("jiage2", null);}else{pmap.put("jiage2", jiage2);}
		if(beizhu==null||beizhu.equals("")){pmap.put("beizhu", null);}else{pmap.put("beizhu", beizhu);}
		
		
		int total=kechengxinxiService.getCount(pmap);
		pageBean.setTotal(total);
		List<Kechengxinxi> list=kechengxinxiService.getByPage(pmap);
		map.put("page", pageBean);
		map.put("list", list);
		session.setAttribute("p", 1);
		return "kechengxinxi_list2";
	}
	
	
	@RequestMapping("kcxxList.do")
	public String kcxxList(@RequestParam(value="page",required=false)String page,
			ModelMap map,HttpSession session,Kechengxinxi kechengxinxi, String bianhao, String kemu, String chexing, String chepai, String yuyueshiduan, String keshi1,String keshi2, String jiage1,String jiage2, String beizhu, String jiaolian, String issh){
		if(page==null||page.equals("")){
			page="1";
		}
		PageBean pageBean=new PageBean(Integer.parseInt(page), 15);
		Map<String, Object> pmap=new HashMap<String,Object>();
		pmap.put("pageno", pageBean.getStart());
		pmap.put("pageSize", 15);
		
		
		if(bianhao==null||bianhao.equals("")){pmap.put("bianhao", null);}else{pmap.put("bianhao", bianhao);}
		if(kemu==null||kemu.equals("")){pmap.put("kemu", null);}else{pmap.put("kemu", kemu);}
		if(chexing==null||chexing.equals("")){pmap.put("chexing", null);}else{pmap.put("chexing", chexing);}
		if(chepai==null||chepai.equals("")){pmap.put("chepai", null);}else{pmap.put("chepai", chepai);}
		if(yuyueshiduan==null||yuyueshiduan.equals("")){pmap.put("yuyueshiduan", null);}else{pmap.put("yuyueshiduan", yuyueshiduan);}
		if(keshi1==null||keshi1.equals("")){pmap.put("keshi1", null);}else{pmap.put("keshi1", keshi1);}
		if(keshi2==null||keshi2.equals("")){pmap.put("keshi2", null);}else{pmap.put("keshi2", keshi2);}
		if(jiage1==null||jiage1.equals("")){pmap.put("jiage1", null);}else{pmap.put("jiage1", jiage1);}
		if(jiage2==null||jiage2.equals("")){pmap.put("jiage2", null);}else{pmap.put("jiage2", jiage2);}
		if(beizhu==null||beizhu.equals("")){pmap.put("beizhu", null);}else{pmap.put("beizhu", beizhu);}
		if(jiaolian==null||jiaolian.equals("")){pmap.put("jiaolian", null);}else{pmap.put("jiaolian", jiaolian);}
		
		int total=kechengxinxiService.getCount(pmap);
		pageBean.setTotal(total);
		List<Kechengxinxi> list=kechengxinxiService.getByPage(pmap);
		map.put("page", pageBean);
		map.put("list", list);
		session.setAttribute("p", 1);
		return "kechengxinxilist";
	}
	
	@RequestMapping("deleteKechengxinxi.do")
	public String deleteKechengxinxi(int id,HttpServletRequest request){
		kechengxinxiService.delete(id);
		String url = request.getHeader("Referer");
		return "redirect:"+url;
		//return "redirect:kechengxinxiList.do";
	}
	
	
}
