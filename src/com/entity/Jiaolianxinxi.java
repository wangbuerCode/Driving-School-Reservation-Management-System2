package com.entity;

public class Jiaolianxinxi {
    private Integer id;
	private String jiaolianzhenghao;
	private String mima;
	private String xingming;
	private String zhaopian;
	private String xingbie;
	private String jiaoling;
	private String shouji;
	private String jiaxiao;
	private String beizhu;
	private String issh;
	
    private String addtime;

    

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
	
	public String getJiaolianzhenghao() {
        return jiaolianzhenghao;
    }
    public void setJiaolianzhenghao(String jiaolianzhenghao) {
        this.jiaolianzhenghao = jiaolianzhenghao == null ? null : jiaolianzhenghao.trim();
    }
	public String getMima() {
        return mima;
    }
    public void setMima(String mima) {
        this.mima = mima == null ? null : mima.trim();
    }
	public String getXingming() {
        return xingming;
    }
    public void setXingming(String xingming) {
        this.xingming = xingming == null ? null : xingming.trim();
    }
	public String getZhaopian() {
        return zhaopian;
    }
    public void setZhaopian(String zhaopian) {
        this.zhaopian = zhaopian == null ? null : zhaopian.trim();
    }
	public String getXingbie() {
        return xingbie;
    }
    public void setXingbie(String xingbie) {
        this.xingbie = xingbie == null ? null : xingbie.trim();
    }
	public String getJiaoling() {
        return jiaoling;
    }
    public void setJiaoling(String jiaoling) {
        this.jiaoling = jiaoling == null ? null : jiaoling.trim();
    }
	public String getShouji() {
        return shouji;
    }
    public void setShouji(String shouji) {
        this.shouji = shouji == null ? null : shouji.trim();
    }
	public String getJiaxiao() {
        return jiaxiao;
    }
    public void setJiaxiao(String jiaxiao) {
        this.jiaxiao = jiaxiao == null ? null : jiaxiao.trim();
    }
	public String getBeizhu() {
        return beizhu;
    }
    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu == null ? null : beizhu.trim();
    }
	public String getIssh() {
        return issh;
    }
    public void setIssh(String issh) {
        this.issh = issh == null ? null : issh.trim();
    }
	
	
	
    public String getAddtime() {
        return addtime;
    }
    public void setAddtime(String addtime) {
        this.addtime = addtime == null ? null : addtime.trim();
    }
}
//   设置字段信息
